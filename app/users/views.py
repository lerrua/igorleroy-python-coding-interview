from rest_framework.views import APIView
from rest_framework.response import Response

from app.users.models import User


# We should user viewsets.ViewSet instead of APIView
# but I was out of time
class ListUsers(APIView):
    def get(self, request, format=None):
        # Should be a Serializer 
        users = User.objects.all()
        return Response(users.__dict__())
